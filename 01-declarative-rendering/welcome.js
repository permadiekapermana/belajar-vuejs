// membuat object app
var app = new Vue({
    // id app
    el  : '#app',
    data: {
        greet       : 'Selamat Pagi',
        firstName   : 'Permadi',
        lastName    : 'Eka Permana'
    }
})

// override message pada object app
// app.greet = 'Hello';

var app2 = new Vue({
    el: '#app-2',
    data: {
        message: 'Ini adalah value dari input'
    }
})